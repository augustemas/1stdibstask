import React, {Component} from 'react';
import {Switch, Route} from "react-router-dom";
import './style/App.css';
import ProductList from './components/ProductList';
import DefaultPage from './components/DefaultPage';
import Details from "./components/Details";

class App extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={ProductList}/>
                <Route path="/item/:item_id" component={Details}/>
                <Route component={DefaultPage}/>
            </Switch>
        );
    }
}

export default App;