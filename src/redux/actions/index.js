import {LOAD_ITEMS, LOAD_CURRENT_ITEM, UNLOAD_CURRENT_ITEM} from "../constants/action-types";

export function getData(currentItem = 0, limit = 9) {
    return function (dispatch) {
        return fetch(`/browse?start=${currentItem}&limit=${limit}`)
            .then(response => response.json())
            .then(json => {
                dispatch({type: LOAD_ITEMS, payload: json});
            });
    };
}

export function getItem(id) {
    return function (dispatch) {
        return fetch(`/item/${id}`)
            .then(response => response.json())
            .then(json => {
                dispatch({type: LOAD_CURRENT_ITEM, payload: json});
            });
    }
}

export function removeItem() {
    return {type: UNLOAD_CURRENT_ITEM, payload: {}};
}