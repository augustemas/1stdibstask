import {LOAD_ITEMS, LOAD_CURRENT_ITEM, UNLOAD_CURRENT_ITEM} from "../constants/action-types";

const initialState = {
    items: [],
    totalItems: 0,
    currentItem: 0,
    itemDetailsStatus: 'unloaded',
    itemDetails: {},
    lastVisitedItem: '',
    favouriteItem: {},
};

function rootReducer(state = initialState, action) {
    if (action.type === LOAD_ITEMS) {
        return Object.assign({}, state, {
            items: state.items.concat(action.payload.items),
            totalItems: action.payload.totalItems,
            currentItem: state.items.concat(action.payload.items).length
        });
    }

    if (action.type === LOAD_CURRENT_ITEM) {
        return Object.assign({}, state, {
            itemDetails: action.payload,
            itemDetailsStatus: 'loaded',
            lastVisitedItem: action.payload.id
        });
    }

    if (action.type === UNLOAD_CURRENT_ITEM) {
        return Object.assign({}, state, {
            itemDetailsStatus: 'unloaded',
            itemDetails: action.payload
        });
    }

    return state;
}

export default rootReducer;