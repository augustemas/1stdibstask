import React from 'react';
import {Link} from 'react-router-dom';

const Navbar = (props) => {
    return <header className="header">
        <Link className={`header__button ${props.menuType}`} to={"/"}>&#10216;<span>Home</span></Link>
        <h1>{props.name}</h1>
    </header>;
};

export default Navbar;