import React, {Component} from 'react';
import Navbar from "./Navbar";
import FavouriteSign from './FavouriteSign';
import connect from "react-redux/es/connect/connect";
import {getItem, removeItem} from "../redux/actions";
import PropTypes from 'prop-types';
import DefaultPage from "./DefaultPage";

const mapDispatchToProps = dispatch => {
    return {
        removeItem: () => dispatch(removeItem()),
        getItem: (id) => dispatch(getItem(id))
    };
};

const mapStateToProps = state => {
    return {
        itemDetails: state.itemDetails,
        itemDetailsStatus: state.itemDetailsStatus
    };
};

export class Details extends Component {

    componentDidMount() {
        this.props.getItem(this.props.match.params.item_id);
    }

    componentWillUnmount() {
        this.props.removeItem();
    }

    render() {

        const {image, title, price, measurements, description, creators, sold, on_hold, seller} = this.props.itemDetails;
        let view;

        if (Object.keys(this.props.itemDetails).length === 0 && this.props.itemDetails.constructor === Object && this.props.itemDetailsStatus === 'loaded') {
            view = <DefaultPage/>;
        } else {
            view = <React.Fragment>
                <main className="main__item">
                    <div className="image-wrapper">
                        <img src={image} alt="product"/>
                        <FavouriteSign/>
                    </div>
                    <div className="info-block">
                        <div className="info-block__wrapper">
                            <h3 className="info-block__title">{title}</h3>
                            <p className="info-block__price">{price ? price.amounts.USD : 'Price Upon Request'}</p>
                            <p className="info-block__measurements"><span
                                className="info-block__label">Measurements:</span><br/>{measurements ? `${measurements.display}` : 'not provided'}
                            </p>
                            <div className="info-block__buttons-wrapper">
                                <div className={`info-block__button ${sold || on_hold ? 'disabled' : ''}`}>PURCHASE
                                </div>
                                <div className={`info-block__button ${sold ? 'disabled' : ''}`}>MAKE OFFER</div>
                            </div>
                        </div>
                        <div className="info-block__wrapper">
                            <p className="info-block__description">{description}</p>
                            <p className="info-block__creators"><span
                                className="info-block__label">Creator: </span>{creators}</p>
                        </div>
                    </div>
                </main>
            </React.Fragment>
        }

        return (
            <React.Fragment>
                <Navbar name={seller ? seller.company : ''} menuType="type"/>
                {view}
            </React.Fragment>
        );
    }
}

Details.propTypes = {
    itemDetails: PropTypes.shape({
        id: PropTypes.string,
        title: PropTypes.string,
        creators: PropTypes.string,
        image: PropTypes.string,
        description: PropTypes.string,
        price: PropTypes.shape({
            amounts: PropTypes.shape({
                EUR: PropTypes.string,
                GBP: PropTypes.string,
                USD: PropTypes.string
            })
        }),
        seller: PropTypes.shape({
            company: PropTypes.string
        }),
        measurements: PropTypes.shape({
            display: PropTypes.string
        }),
        sold: PropTypes.bool,
        on_hold: PropTypes.bool,
    }).isRequired
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Details);