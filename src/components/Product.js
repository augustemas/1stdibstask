import React from 'react';
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';
import FavouriteSign from "./FavouriteSign";

const Product = (props) => {

    const {id, image, price} = props.product;

    return (
        <div className={`product-card restore-${id}`}>
            <div className="image-container">
                <Link to={`/item/${id}`}>
                    <img src={image} alt="product"/>
                </Link>
            </div>
            <div className="card-footer">
                <div className="card-footer__price">{price ? price.amounts.USD : 'Price Upon Request'}</div>
                <FavouriteSign/>
            </div>
        </div>
    );
};

Product.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.string,
        image: PropTypes.string,
        price: PropTypes.shape({
            amounts: PropTypes.shape({
                EUR: PropTypes.string,
                GBP: PropTypes.string,
                USD: PropTypes.string
            })
        })
    }).isRequired
};

export default Product;