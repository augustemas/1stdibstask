import React from 'react';

const LoadButton = (props) => {
    return (
        <React.Fragment>
            <div onClick={() => props.onClickLoad()}
                 className={`load-button ${props.disabled === true ? 'disabled' : ''}`}>LOAD MORE
            </div>
        </React.Fragment>
    );
};

export default LoadButton;