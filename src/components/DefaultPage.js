import React from 'react';

const DefaultPage = () => {
    return <div><h3>Page not found</h3></div>;
};

export default DefaultPage;