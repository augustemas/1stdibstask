import React, { Component } from 'react';
import Product from './Product';
import Navbar from './Navbar';
import LoadButton from './LoadButton';
import { connect } from "react-redux";
import { getData } from "../redux/actions/index";

export class ProductList extends Component {
    constructor(props){
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        if(this.props.items.length === 0) this.props.getData();

        const item = document.querySelector(`.restore-${this.props.lastVisitedItem}`);
        if (item) setTimeout(() => {item.scrollIntoView()}, 100);
    }

    handleClick(){
        if(this.props.currentItem >= this.props.totalItems) return;

        let limit = this.props.currentItem + 9 > this.props.totalItems ? this.props.totalItems - this.props.currentItem : 9;
        this.props.getData(this.props.currentItem, limit);
    }

    render() {
        return (
            <React.Fragment>
                <Navbar name="Browse Page" menuType="browse"/>
                <main className="main">
                    {this.props.items.map((item) => (
                        <Product key={item.id} product={item}/>
                    ))}
                     <div className="button-wrapper">
                         <LoadButton onClickLoad={() => {this.handleClick();}} disabled={this.props.currentItem >= this.props.totalItems}/>
                     </div>
                </main>
            </React.Fragment>
        );
    }
}
function mapStateToProps(state) {
    return {
        items: state.items,
        currentItem: state.currentItem,
        totalItems: state.totalItems,
        lastVisitedItem: state.lastVisitedItem
    };
}
export default connect(
    mapStateToProps,
    {getData}
)(ProductList);